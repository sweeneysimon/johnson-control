import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-full-photo',
  templateUrl: './full-photo.component.html',
  styleUrls: ['./full-photo.component.css']
})

/*
 full-photo component

 Takes a single photo object as an input to display a fullscreen image
*/
export class FullPhotoComponent implements OnInit {

  @Input() selectedPhoto: Object;
  @Output() public close = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  // sets the the selectedPhoto in home to null
  onClosePhoto() {
    this.close.emit();
  }
}
