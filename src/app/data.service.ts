import { Injectable } from '@angular/core';;

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // used to allow unsplashed api use
  client_id: String = '07c675ddb079b76cdd8e41350ef2f0c8fae5d042243f36f67b5f979f0ef01564';
  initalQuery: String = 'internet+of+things';


  constructor() { }

  // makes the initial call to return images with the query 'internet of things'
  initialAPICall() {
    return fetch(`https://api.unsplash.com/search/photos?client_id=${this.client_id}&query=${this.initalQuery}`,{method:'get'});
  }

  // makes a call based on what the user wants to search for
  userDefinedQuery(value) {
    let userQuery = value.trim().replace(/[^a-zA-Z ]/g, "").split(' ').join('+');
    return fetch(`https://api.unsplash.com/search/photos?client_id=${this.client_id}&query=${userQuery}`,{method:'get'});
  }
}
