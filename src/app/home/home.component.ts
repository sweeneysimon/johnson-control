import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

/*
Main component
*/

export class HomeComponent implements OnInit {

  photos: Object;
  selectedPhoto: Object;
  userSearchQuery: String;

  constructor(private data: DataService) { }
  // get the initial list of photos to be displayed 'internet of things'
  ngOnInit() {
    this.data.initialAPICall()
    .then(res => res.json())
    .then(res => {
      this.photos = res;
    }).catch(error => {
      console.log(error);
    });
  }

  // get the user inputted search result from unsplashed
  onUserSearch() {
    this.data.userDefinedQuery(this.userSearchQuery)
    .then(res => res.json())
    .then(res => {
      this.photos = res;
    }).catch(error => {
      console.log(error);
    });
  }

  // set the user inputted search query
  updateUserQuery(value) {
    this.userSearchQuery = value;
  }

  // unsets the user selected photo
  closeSelectedPhoto() {
    this.selectedPhoto = null;
  }

}
