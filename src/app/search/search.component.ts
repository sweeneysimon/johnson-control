import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

/*
  search component

  Provides an input to allow the user to preform a search
*/
export class SearchComponent implements OnInit {

  @Input() userSearchQuery: String;
  @Output() public userSearch = new EventEmitter();
  @Output() public userSearchInput = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  // if the user hits the enter key or presses the search icon
  // make a search
  onUserSearch(event) {
    if (event.key === "Enter" || event === 'search') {
      this.userSearch.emit();
    }
  }

  // set the users input as the userSearchQuery in home
  setQueryText(event) {
    if (event.target) {
      this.userSearchInput.emit(event.target.value);
    }
  }

}
