import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
/*
 Photos component
 Displays the list of photos from unsplash

 Takes an object containing an array of photos as input from the home component
 Returns a single photo object as an output
*/
export class PhotosComponent implements OnInit {

  @Input() photos: Object;
  @Output() public selected = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  // returns a photo object to the home component
  // when the user clicks on a photo to view fullscreen
  onClickPhoto(value) {
    this.selected.emit(value);
  }

}
